import React from 'react'
import PropTypes from 'prop-types'
import { Drawer, Button } from 'antd';

import logo from '../assets/logobrand.png'

const Navbar = props => {
    return (
        <nav className="menuBar">
            <div className="logo">
                <img src={logo} width={100} height={100}></img>
            </div>

        </nav>
    )
}



export default Navbar