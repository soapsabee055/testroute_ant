import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Layout , Form, Input, Button, Checkbox , Row , Col } from 'antd';
import { Card } from 'antd';
import { Link} from "react-router-dom";

const { Footer, Sider, Content } = Layout;


const onFinish = (values) => {
    console.log('Success:', values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };


export class Home extends Component {
    static propTypes = {

    }

    

    render() {
        return (
            <Layout>

                <Row type="flex" justify="center" align="middle" style={{minHeight: '100vh'}}>
                <Col span={12} offset={6}>
                <Content >
              
                    <Card title="Login" bordered={false} style={{ width: 300 }}>
                        <Form
                            name="basic"
                            labelCol={{
                                span: 8,
                            }}
                            wrapperCol={{
                                span: 16,
                            }}
                            initialValues={{
                                remember: true,
                            }}
                            onFinish={onFinish}
                            onFinishFailed={onFinishFailed}
                        >
                            <Form.Item
                                label="Username"
                                name="username"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your username!',
                                    },
                                ]}
                            >
                                <Input />
                            </Form.Item>

                            <Form.Item
                                label="Password"
                                name="password"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your password!',
                                    },
                                ]}
                            >
                                <Input.Password />
                            </Form.Item>

                            <Form.Item
                                name="remember"
                                valuePropName="checked"
                                wrapperCol={{
                                    offset: 8,
                                    span: 16,
                                }}
                            >
                                <Checkbox>Remember me</Checkbox>
                            </Form.Item>

                            <Form.Item
                                wrapperCol={{
                                    offset: 8,
                                    span: 16,
                                }}
                            >
                                <Link to="/dashboard">
                                <Button type="primary" htmlType="submit"> 
                                Login
                                </Button>
                                </Link>
                            </Form.Item>
                        </Form>
                    
                 </Card>
                              
                </Content>
                </Col>
            </Row>                
                <Footer></Footer>
            </Layout>

        )
    }
}

export default Home
