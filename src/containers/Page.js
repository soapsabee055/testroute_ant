import React from 'react'
import { Link} from "react-router-dom";
import {Button} from 'antd';

const Page = props => {
    return (
        <div>
            <Link to="/">
            <Button type="primary">กลับไปหน้าแรก</Button>
            </Link>
            
        </div>
    )
}


export default Page
