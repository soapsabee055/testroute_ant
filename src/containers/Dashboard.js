import React, { useState, useEffect } from 'react'
import { Layout, Button, Modal, Input, Table, Tag, Space, Form, Row, Pagination } from 'antd';
import { v4 as uuidv4 } from 'uuid';

const { Sider, Content } = Layout;

const { Column, ColumnGroup } = Table;




const Dashboard = props => {

    const [selectedRowKeys, setSelectedRowKeys] = useState([])
    const [objdata, setData] = useState([])
    const [objResult, setResult] = useState([])
    const [visible, setVisible] = useState(false);
    const [btnSaveVisible, setBtnSaveVisible] = useState(false)

    const [modal, setModal] = useState({})

    const [handle, setHandle] = useState({
        key: "",
        firstName: "",
        lastName: "",
        age: "",
        address: ""
    })

    const [] = useState([])

    const openModalType = (obj) => {

        setVisible(true)

        if (obj.type === "Edit") {
            placeholderEdit(obj.value)
            return setModal({
                title: "Edit Data",
                type: "Edit",

            })
        }

        return setModal({
            title: "Add Data",
            type: "Add"
        })

    }

    const addData = (e) => {

        handle.key = new Date().valueOf();
        setData([...objdata, handle])
        setHandle({})
        setVisible(false)

    }

    const placeholderEdit = (e) => {
        const { record } = e
        let obj = {
            key: record.key,
            firstName: record.firstName,
            lastName: record.lastName,
            age: record.age,
            address: record.address
        }
        setHandle(obj)

    }

    const editData = (e) => {
        objdata.filter(ele => {
            if (ele.key === handle.key) {
                ele.firstName = handle.firstName
                ele.lastName = handle.lastName
                ele.age = handle.age
                ele.address = handle.address
            }
        })
        setData([...objdata])
        setHandle({})
        setVisible(false)
    }

    const deleteData = (e) => {
        const removeIndex = objdata.findIndex(item => item.key == e);
        if (removeIndex > -1) {
            objdata.splice(removeIndex, 1)
            setData([...objdata])
        }
    }

    const deleteResult = (e) => {
        const removeIndex = objResult.findIndex(item => item.key == e);
        if (removeIndex > -1) {
            objResult.splice(removeIndex, 1)
            setResult([...objResult])
        }
    }

    const handleData = (e) => {
        let obj = {
            key: handle.key || "",
            firstName: handle.firstName || "",
            lastName: handle.lastName || "",
            age: handle.age || "",
            address: handle.address || ""
        }

        obj[e.key] = e.value

        setHandle(obj)
    }

    const modalCancel = () => {
        setHandle({})
        setVisible(false)
    }


    const onSelectChange = (e) => {
        setSelectedRowKeys(e)
    }


    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange
    };

    const checkSelected = () => {
        if (selectedRowKeys.length > 0) {
            return setBtnSaveVisible(true)
        }
        return setBtnSaveVisible(false)
    }

    const saveToAnother = () => {
        selectedRowKeys.map(e => {
           let result = objdata.find(o => o.key === e ) 
           let obj = {}
           let temp = Object.assign(obj,result)
           Object.assign(temp, { key: uuidv4()});
            if (result !== undefined ) {
                objResult.push(temp)
                setResult([...objResult])
            }
        })


        setSelectedRowKeys([])


    }


    useEffect(() => {

        checkSelected()

    }, [objdata, objResult, handle, selectedRowKeys])


    return (

        <Layout>

            <Modal
                title={modal.title}
                centered
                visible={visible}
                onOk={() => modal.type === "Add" ? addData() : editData()}
                okText={"Submit"}
                onCancel={() => modalCancel()}
                width={1000}
            >

                <Space direction="vertical">

                    <Row>
                        <Space>
                            <Form.Item label="First Name" >
                                <Input placeholder="input placeholder" value={handle.firstName} onChange={(e) => handleData({ key: "firstName", value: e.target.value })} />
                            </Form.Item>
                            <Form.Item label="Last Name">
                                <Input placeholder="input placeholder" value={handle.lastName} onChange={(e) => handleData({ key: "lastName", value: e.target.value })} />
                            </Form.Item>
                            <Form.Item label="Age">
                                <Input placeholder="input placeholder" value={handle.age} onChange={(e) => handleData({ key: "age", value: e.target.value })} />
                            </Form.Item>
                        </Space>
                    </Row>

                    <Form.Item label="Address">
                        <Input placeholder="input placeholder" value={handle.address} onChange={(e) => handleData({ key: "address", value: e.target.value })} />
                    </Form.Item>



                </Space>

            </Modal>
            <Layout>

                <Content style={{ padding: '100px 100px' }}>
                    <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                        <Space>

                            {btnSaveVisible === true && <Button type="primary" style={{ marginBottom: 16 }} onClick={() => saveToAnother()}>Save</Button>}


                            <Button type="primary" style={{ marginBottom: 16 }} onClick={() => openModalType({ type: "Add", value: null })}>
                                Add a row
                            </Button>
                        </Space>
                    </div>
                    <Table rowSelection={rowSelection} dataSource={objdata} pagination={{ pageSize: 5 }}>
                        <Column title="First Name" dataIndex="firstName" key="firstName" />
                        <Column title="Last Name" dataIndex="lastName" key="lastName" />
                        <Column title="Age" dataIndex="age" key="age" />
                        <Column title="Address" dataIndex="address" key="address" />
                        <Column key="action" render={(text, record, index) => (
                            <Button key="key" type="primary" onClick={() => openModalType({ type: "Edit", value: { index: index, record: record } })}>EDIT</Button>
                        )} />
                        <Column key="action" render={(text, record) => (

                            <Button key="key" danger onClick={() => deleteData(text.key)}>DELETE</Button>
                        )} />
                    </Table>
                </Content>

            </Layout>

            <Layout>

                <Content style={{ padding: '100px 100px' }}>

                    <Table dataSource={objResult} pagination={{ pageSize: 5 }}>
                        <Column title="First Name" dataIndex="firstName" key="firstName" />
                        <Column title="Last Name" dataIndex="lastName" key="lastName" />
                        <Column title="Age" dataIndex="age" key="age" />
                        <Column title="Address" dataIndex="address" key="address" />

                        <Column key="action" render={(text, record) => (

                            <Button key="key" danger onClick={() => deleteResult(text.key)}>DELETE</Button>
                        )} />
                    </Table>
                </Content>

            </Layout>
        </Layout>

    )
}

export default Dashboard
