import logo from './logo.svg';
import './App.css';
import Navbar  from './components/Navbar'

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  withRouter 
} from "react-router-dom";
import { Home , Page , Dashboard  } from './containers/';

function App() {
  return (
    <Router>
     <Navbar/>
        <Switch>
       
          <Route exact path="/" component={withRouter(Home)}>
          </Route>

          <Route exact path="/page" component={withRouter(Page)}>
          </Route>

          <Route exact path="/dashboard" component={withRouter(Dashboard)}>
          </Route>
          

        </Switch>
    </Router>
  );
}

export default App;
